package be.bruxelleformation.demo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.BiFunction;

public enum AccountType {
    CURRENT(0.03, (amount, nb) -> 0.03 * amount),
    SAVING(0.045, (amount, nb) -> 0.045 * amount);

    public double taux;
    public BiFunction<Double, Double, Double> interet;

    AccountType(double taux, BiFunction<Double, Double, Double> calculInteret) {
        this.taux = taux;
        this.interet = calculInteret;

        try (BufferedReader reader = new BufferedReader(new FileReader("demo.json"))) {

        } catch (IOException e) {

        }

    }

}
