package be.bruxelleformation.demo;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Dice {

    public static void main(String[] args) {
        System.out.println(DiceType.D6.rollForState());
    }

    public enum DiceType {
        D4(4),
        D6(6),
        D12(12);

        private final int maximum;

        DiceType(int maximum) {
            this.maximum = maximum;
        }

        public int roll() {
            return new SecureRandom().nextInt(0, maximum) + 1;
        }

        public int rolls(int nbDice, int keep) {
            List<Integer> rolls = new ArrayList<>();

            for (int i = 0; i < nbDice; i++) {
                rolls.add(roll());
            }

            return rolls.stream()
                    .mapToInt(it -> it)
                    .unordered()
                    .limit(keep)
                    .sum();
        }

        public int rollForState() {
            return rolls(4, 3);
        }
    }
}
