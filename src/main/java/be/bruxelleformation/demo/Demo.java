package be.bruxelleformation.demo;

import be.bruxelleformation.models.Person;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicReference;

public class Demo {
    public static void main(String[] args) {
//        Long[] array = {10L, 9L, 8L, 7L, 6L, 5L, 4L, 3L, 2L, 1L, 0L};
        String[] array = {"Flavian", "Blop", "Biblop", "blop"};
        //region ASC
        sort(array, (t1, t2) -> t1.compareTo(t2) > 0);
        //endregion
        for (String i : array) {
            System.out.println(i);
        }
        //region DESC
        sort(array, (t1, t2) -> t1.compareTo(t2) < 0);
        //endregion
        for (String it : array) {
            System.out.println(it);
        }

        Person[] people = {
                new Person("Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
                new Person("Ovyn1", "Flavian", LocalDate.of(1992, 7, 19)),
                new Person("Ovyn0", "Flavian", LocalDate.of(1991, 7, 20)),
                new Person("Ovyn74", "Flavian", LocalDate.of(1991, 8, 19))
        };

        AtomicReference<String> toCompare = new AtomicReference<>("Blop");
        sort(people, (p1, p2) -> {
            toCompare.set("Biblop");
            return p1.getLastname().compareTo(p2.getLastname()) < 0;
        });
        for (Person p : people) {
            System.out.println(p);
        }
    }

    public static <T> void sort(T[] array, ISort<T> sort) {
        for (int i = 0; i < array.length - 1; i++) {
            int iPredicate = i;
            for (int j = i + 1; j < array.length; j++) {
                T nb1 = array[iPredicate];
                T nb2 = array[j];
                if (!sort.compare(nb1, nb2)) {
                    iPredicate = j;
                }
            }
            if (iPredicate != i) {
                T tmp = array[i];
                array[i] = array[iPredicate];
                array[iPredicate] = tmp;
            }
        }
    }

    public enum OrderType {
        ASC,
        DESC
    }

    public interface ISort<T> {
        boolean compare(T t1, T t2);
    }
}
