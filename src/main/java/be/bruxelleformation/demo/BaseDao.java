package be.bruxelleformation.demo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class BaseDao<TEntity extends Entity<TKey>, TKey> {
    private final List<TEntity> entities = new ArrayList<>();

    public Optional<TEntity> findById(TKey id) {
        return this.entities.stream()
                .filter(it -> it.id.equals(id))
                .findFirst();
    }

    public Collection<TEntity> findAll() {
        return this.entities;
    }
}
