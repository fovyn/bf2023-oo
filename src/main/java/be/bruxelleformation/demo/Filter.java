package be.bruxelleformation.demo;

import java.util.*;

public class Filter {


    public static void main(String[] args) {
//        List<String> collection = new ArrayList<>();
//        collection.addAll(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Flavian");
        map.put(2, "Aurélien");

        List<Integer> filtered = filter(map.keySet(), (it) -> it > 1).stream().toList();

        filtered.forEach(System.out::println);
    }

    public static <T> Collection<T> filter(Collection<T> collection, IFilter<T> filter) {
        List<T> filtered = new ArrayList<>();

        for (T item : collection) {
            if (filter.execute(item)) {
                filtered.add(item);
            }
        }

        return filtered;
    }

    public interface IFilter<T> {
        boolean execute(T item);
    }
}
