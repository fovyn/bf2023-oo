package be.bruxelleformation.demo.stream;

import be.bruxelleformation.models.account.Account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class Demo {

    public static void main(String[] args) {
        List<Account> accounts = new ArrayList<>();
        List<String> accountNumbers = accounts.stream()
                .map(Account::getNumber)
                .toList();

        Collection<Double> a2 = map(accounts, (a) -> a.getAmount());

        List<Integer> ints = Arrays.asList(1, 2, 3, 4, 5, 7, 8, 9, 10);
//        Collection<Integer> m = map(ints, (it) -> it * it);

        CStream<Integer> stream = new CStream<>(ints);
        List<Integer> res = stream
                .map((it) -> it * it)
                .toList();

        String str = "";
        str
                .toLowerCase()
                .substring(0, 2)
                .toUpperCase();
    }

    public static <TIn, TOut> Collection<TOut> map(Collection<TIn> collection, Function<TIn, TOut> mapFunction) {
        List<TOut> outCollection = new ArrayList<>();

        for (TIn it : collection) {
            outCollection.add(mapFunction.apply(it));
        }

        return outCollection;
    }
}
