package be.bruxelleformation.demo.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class CStream<T> {
    private final Collection<T> collection;

    public CStream(Collection<T> collection) {
        this.collection = collection;
    }

    public <K> CStream<K> map(Function<T, K> mapping) {
        List<K> outCollection = new ArrayList<>();

        for (T it : collection) {
            outCollection.add(mapping.apply(it));
        }

        return new CStream<>(outCollection);
    }

    public List<T> toList() {
        List<T> list = new ArrayList<>();

        list.addAll(this.collection);

        return list;
    }
}
