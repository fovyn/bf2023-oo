package be.bruxelleformation.models;

public interface ICustomer {
    double getAmount();

    Person getOwner();

    void deposit(double amount);

    void withdrawal(double amount);
}
