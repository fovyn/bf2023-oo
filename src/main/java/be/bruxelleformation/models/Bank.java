package be.bruxelleformation.models;

import be.bruxelleformation.exceptions.AccountExistException;
import be.bruxelleformation.exceptions.AccountNotExistException;
import be.bruxelleformation.models.account.Account;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;

/**
 * Mutable class to represent a bank
 * FA: Bank[name]
 *
 * @attribute name String
 * @attribute accounts Map
 * @invariant name != null && name.size > 0
 * @invariant accounts != null
 */
public class Bank implements Iterable<Account>, Closeable {
    private final String name;
    private Map<String, Account> accounts = new HashMap<>();

    /**
     * @param name String | name != null
     * @throws NullPointerException |name == null
     * @throws RuntimeException     | name.size == 0
     */
    public Bank(String name) {
        if (name == null) throw new NullPointerException("Bank(String): name cannot be null");
        if (name.length() == 0) throw new RuntimeException("Bank(String): name.size must be gt 0");
        this.name = name;
    }

    public Bank(Bank bank) {
        this.name = bank.name;
        this.accounts = new HashMap(bank.accounts); //Deep copy
//        this.accounts = bank.accounts; //Shallow Copy
    }

    public String getName() {
        return name;
    }

    /**
     * @param number String | number != null
     * @return Account
     * @throws NullPointerException     |  number == null
     * @throws AccountNotExistException | !accounts.contains(number)
     */
    public Account get(String number) throws AccountNotExistException {
        if (number == null) throw new NullPointerException("Bank.get(String): number cannot be null");
        if (!accounts.containsKey(number))
            throw new AccountNotExistException("Bank.get(String): number is not associated with an account");
        return accounts.get(number);
    }

    /**
     * @param account Account | account != null && account.number != null
     * @throws NullPointerException  |account == null || account.number == null
     * @throws AccountExistException | accounts.containsKey(number) == true
     * @modify this.accounts | this.accounts.size => this.accounts.size + 1
     */
    public void addAccount(Account account) {
        if (accounts.containsKey(account.getNumber())) return;

        accounts.put(account.getNumber(), account);
    }

    /**
     * @param number String | number != null
     * @throws AccountNotExistException | accounts.containsKey(number) == true
     * @modify this.accounts | this.accounts.size => this.accounts.size - 1
     */
    public void removeAccount(String number) {
        if (!accounts.containsKey(number)) return;

        accounts.remove(number);
    }

    /**
     * @param owner Person | owner != null
     * @return sum(accounts.ownBy ( owner).amount)
     * @throws NullPointerException | owner == null
     */
    public double totalAmount(Person owner) {
        return this.accounts.values().stream()
                .filter(it -> it.getOwner().equals(owner))
                .mapToDouble(Account::getAmount)
                .filter(it -> it > 0)
                .sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return name.equals(bank.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Bank.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }

    public int getLength() {
        return this.accounts.size();
    }

    @Override
    public Iterator<Account> iterator() {
        return new AccountIterator();
    }

    @Override
    public void close() throws IOException {
        this.accounts.clear();
    }

    private class AccountIterator implements Iterator<Account> {
        private int currentIndex = 0;


        @Override
        public boolean hasNext() {
            return currentIndex < getLength();
        }

        @Override
        public Account next() {
            return accounts.values().stream().toList().get(currentIndex++);
        }
    }
}
