package be.bruxelleformation.models;

public interface IBanker extends ICustomer {
    void interestApply();

    default void sayHello() {
        System.out.println("Hello there");
    }
}
