package be.bruxelleformation;

import be.bruxelleformation.demo.AccountDao;
import be.bruxelleformation.models.Bank;
import be.bruxelleformation.models.Person;
import be.bruxelleformation.models.account.CurrentAccount;
import be.bruxelleformation.models.account.SavingAccount;

import java.io.IOException;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Person owner = new Person("Ovyn", "Flavian", LocalDate.of(1991, 7, 19));

        CurrentAccount ca = new CurrentAccount("BE12 1234 1234 1234", owner, 50, 50);
        SavingAccount sa = new SavingAccount("BE12 1234 1234 1235", owner, 50);
        Bank bank = new Bank("Picsou");
        try (bank) {
            bank.addAccount(ca);
            bank.addAccount(sa);


            AccountDao dao = new AccountDao();
            dao.findById("BE12 1234 1234 1234");
            System.out.println(bank.getLength());
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println(bank.getLength());
    }
}