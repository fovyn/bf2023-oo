package be.bruxelleformation.exceptions;

public class AccountExistException extends Exception {
    public AccountExistException(String s) {
        super(s);
    }
}
