package be.bruxelleformation.exceptions;

public class AccountNotExistException extends Exception {
    public AccountNotExistException(String s) {
        super(s);
    }
}
