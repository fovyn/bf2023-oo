package be.bruxelleformation.models;

import be.bruxelleformation.exceptions.AccountNotExistException;
import be.bruxelleformation.models.account.Account;
import be.bruxelleformation.models.account.CurrentAccount;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class BankTest {

    @Test
    void getNameTest() {
        assertEquals("Picsou", new Bank("Picsou").getName());
    }

    @Test
    void getNameNullTest() {
        assertThrowsExactly(NullPointerException.class, () -> {
            String name = null;
            new Bank(name);
        });
    }

    @Test
    void getNameEmpty() {
        assertThrows(Exception.class, () -> new Bank(""));
    }

    void assertDemoThrows(Class<?> exception, Executable run) {
        try {
            run.execute();
            fail();
        } catch (Throwable e) {
            if (!e.getCause().equals(exception)) {
                fail();
            }
        }
    }

    @Test
    void getAccountWithEmptyBank() {
        assertThrowsExactly(AccountNotExistException.class, () -> new Bank("Picous").get("BE12 1234 1234 1234"));
    }

    @Test
    void getAccountWithNullNumber() {
        assertThrowsExactly(NullPointerException.class, () -> {
            String number = null;
            new Bank("Picsou").get(number);
        });
    }

    @Test
    void getAccountNotExist() {
        Bank b = new Bank("Picsou");
        b.addAccount(new CurrentAccount("BE12 1234 1234 1234", new Person("Ovyn", "Flavian", LocalDate.of(1991, 7, 19))));

        assertThrowsExactly(AccountNotExistException.class, () -> b.get("BE12 1234 1234 1235"));
    }

    @Test
    void getAccountExist() throws AccountNotExistException {
        Bank b = new Bank("Picsou");
        Account account = new CurrentAccount("BE12 1234 1234 1234", new Person("Ovyn", "Flavian", LocalDate.of(1991, 7, 19)));
        b.addAccount(account);

        assertEquals(account, b.get("BE12 1234 1234 1234"));
    }

    @Test
    void addAccount() {
        Bank b = new Bank("Picsou");
        Account account = new CurrentAccount("BE12 1234 1234 1234", new Person("Ovyn", "Flavian", LocalDate.of(1991, 7, 19)));
        assertEquals(0, b.getLength());
        b.addAccount(account);
        assertEquals(1, b.getLength());
    }
}
